# Aplikacja

Wzór aplikacji Pythonowej wykorzystywanej na Hackwaw Disrupt - Warszawa 2 kwietnia.

# Sposoby odpalenia

## Docker

Zbuduj obraz: `docker build -t disrupt-python-app`
Uruchom obraz: `docker run -it disrupt-python-app`

## Bez kontenera

Stwórz virtualenv. Następnie zainstaluj zależności: `pip install -r requirements`
Po tym możesz uruchomić aplikację web:

```python app.py web```

lub przerzucanie tweetów z Twitter proxy do Slack proxy:

```python app.py pipe```

Do uruchomienia potrzebna jest java i maven. Uruchom komendę mvn spring-boot:run
